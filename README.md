# toki pi jan Lentan

jan Inkepa (Increpare) li pali e nasin sitelen ni. taso, ona li weka e ona tan
lipu ona. mi awen e nasin sitelen ni kepeken lipu Wayback Machine.

(Increpare made this font, but removed it later. I managed to find a copy of the
git repo with Wayback Machine.)

# linja_pimeja
ni li nasin sitelen pi [sitelen pona](http://tokipona.net/tp/janpije/hieroglyphs.php).

o kama jo e ona lon [lipu ni](https://github.com/increpare/linja_pimeja/releases).

sina kama e sona pi pakala ona, la o toki tawa mi lon lipu Issues :)

![linja pimeja sample](nimi_ale.png)
